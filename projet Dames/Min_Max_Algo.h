/**
 * \fichier min-max.h
 *  \ Implémentation du min-max avec élagage Alpha-Bêta
 */

#define MINIMUM -1
#define MAXIMUM 1

#include "fonction_evaluation.h"

/**
 * \fn coup jouerIA(const plateau p, int profondeur);
 *
 * \ Renvoie le meilleur coup que peut jouer le joueur courant.
 * \param profondeur La profondeur à laquelle on doit utiliser la fonction d'évaluation. (profondeur > 0)
 */
coup jouerIA(const plateau p, int profondeur);

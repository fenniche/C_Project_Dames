#include <stdio.h>
#include <stdlib.h>
//#include <conio.h>
//#include <windows.h>
#include <time.h>
#include "partie_humainVshumain.h"




    // PROTOTYPES



// variables globales pour pouvoir les reutiliser dans les autres fonction comme jeu()
//char* joueur1 = NULL; // initialisation de l'allocation memoire
//char* joueur2 = NULL; // initialisation de l'allocation memoire
    	char joueur1[20];
	char joueur2[20];
	int pion;
	int fin_de_partie =0;
	int Damier2[10][10];


void color(int t,int f)
{
//    HANDLE H=GetStdHandle(STD_OUTPUT_HANDLE);
//    SetConsoleTextAttribute(H,f*16+t);
}


// menu du debut
void menu()
{

    char couleur_joueur1[5];
    char couleur_joueur2[5];
    int ligne=0;
    int colonne;
    int taille_ligne =10;
    int taille_colonne =10;

    char case_blanche=200;
//joueur1 = malloc(sizeof(char)); // allocation de la memoire
//joueur2 = malloc(sizeof(char)); // allocation de la memoire
if (joueur1 == NULL || joueur2 == NULL)
{
exit(0);
}
/*
printf("Jeu de dame en C\n");
color(0,15);
printf("Joueur 1 :");
color(15,0);
printf(" ");
scanf("%s", joueur1);
color(0,15);
printf("Joueur 2 :");
color(15,0);
printf(" ");
scanf("%s", joueur2);
printf("\n\n");
color(11,0);*/


  printf("\n Vous avez choisi de commencer une nouvelle partie (contre humain)\n");
               printf("\n Quel est votre nom (joueur 1) ? \n");
               scanf("%s",&joueur1);
               printf("\n Quel est votre nom (joueur 2) ? \n");
               scanf("%s",&joueur2);

               printf("\n%s, Qu'elle couleur voulez-vous prendre : les blancs ou les noirs ? (blancs/noirs)\n",joueur1);
               scanf("%s",&couleur_joueur1);
		printf("\n");
               printf("%s a pris les %s\n\n\n\n",joueur1,couleur_joueur1);

               system("cls");
               printf("Chargement du damier en cours ... \n\n");
             	 //Sleep(1000);
            	system("cls");
                printf(" %s ", joueur1);
                color(15,0);
                printf(" VS ");
                color(12,0);
                printf(" %s \n\n\n", joueur2);
                color(15,0);
}



// initialisation du plateau de jeux (tableau 2D) a 0
void initialisation_plateau(int Damier[10][10])
{
int i,j;
for (i=0;i<10; i++)
{
for(j=0; j<10;j++)
{
Damier[i][j] = 0;
}
}
}



// configuration pour le placement des pions sur le damier
void configuration_piont(int Damier[10][10])
{
int i,j;
// premiere boucle pour les colonnes allant de 0 a 4 car il y a que des
for(i=0;i<4;i++)
{
for(j=0;j<10;j++)
{
if((i+j)%2 == 0) // pour alterner 1 fois sur 2
{
Damier[i][j] = 0; // on met la valeur a 0
}
else
{
Damier[i][j] = 2; // on met a 2 ce qui correspond au NOIRS
}
}

}


// pour la deuxieme partie du damier de la 4 eme a la 6 eme colonne car il n'y a pas de pions dans cette partie
for(i=4;i<6;i++)
{
for(j=0;j<10;j++)
{
Damier[i][j] = 0; // font blanc sans pions
}

}

// derniere partie du damier ou il y a que des noirs
for(i=6;i<10;i++)
{
for(j=0;j<10;j++)
{
if((i+j)%2 == 0)
{
Damier[i][j] = 0;
}
else
{
Damier[i][j] = 1; // B
}
}

}


}


// affichage du damier
void afficher_plateau(int Damier[10][10])
{
printf(" 0  1  2  3  4  5  6  7  8  9  ");
printf("\n");
printf("_______________________________");
printf("\n");
int i,j;
for(i=0; i<10; i++)
{
for(j=0;j<10;j++)
{
if(Damier[i][j] == 1) // Si sa vaut 1 on affiche un B
{
color(11,0);
printf("[B]");
color(15,0);
}
else if(Damier[i][j] == 2) // afichage des N
{
color(12,0);
printf("[N]");
color(15,0);
}
else
{
if((j+i)%2 == 0)
{
color(0,15);
printf("[ ]");
color(15,0);
}
else
{
color(15,0);
printf("[ ]");
color(15,0);
}
}
}

printf(" | %3d\n",i);

}
printf("_______________________________");
printf("\n");
printf(" 0  1  2  3  4  5  6  7  8  9  \n\n\n");
}



int verification (int depart_x, int depart_y, int arrivee_x, int arrivee_y, int pion, int damier[10][10])
{
if (pion == 1) // B
{
  if ((damier[arrivee_x][arrivee_y] == 0) && (damier[arrivee_x][arrivee_y] !=1)) // Si la case de destination est vide et qu(il y a pas de blancs dessus
  {
    if (((arrivee_x == depart_x - 1) && (arrivee_y == depart_y - 1)) || ((arrivee_x == depart_x + 1) && (arrivee_y == depart_y - 1))) // condition pour faire que les 2 mouvements possible diagonale gauche et droite d'une seule case
  {
  return 1;
}
else
{
  return 0;
}
}
}
 else
  if (pion == 2) // N
{
  if ((damier[arrivee_x][arrivee_y] == 0 ) && (damier[arrivee_x][arrivee_y] !=2)) // Si la case de destination est vide et qu'il n'y a pas deja un noir dessus
  {
  if (((arrivee_x == depart_x -1 ) && (arrivee_y == depart_y + 1)) || ((arrivee_x == depart_x + 1) && (arrivee_y == depart_y + 1)))
  {
  return 1;
}

else
{
return 0;
}
}
}
else{return 0;} // sinon erreur
}



void jeu(int pion)
{

if (pion == 1) //B
{

int depart_x, depart_y, arrivee_x, arrivee_y;
printf("%s Entrez labcisse de depart du pion : ", joueur1);
scanf("%d", &depart_y);
printf("%s Entrez l'ordonnee de depart du pion : ", joueur1);
scanf("%d", &depart_x);
printf("\n\n");
printf("%s Entrez labcisse d'arrivee du pion : ", joueur1);
scanf("%d", &arrivee_y);
printf("%s Entrez l'ordonnee d'arrivee du pion : ", joueur1);
scanf("%d", &arrivee_x);
printf("\n\n");
if (verification(depart_x, depart_y, arrivee_x, arrivee_y, pion, Damier2) == 1)
{
Damier2[arrivee_x][arrivee_y]=Damier2[depart_x][depart_y];
Damier2[depart_x][depart_y] = 0;
}
//system("cls");
afficher_plateau(Damier2);
}

else
{
int depart_x, depart_y, arrivee_x, arrivee_y;
printf("%s Entrez labcisse de depart du pion : ", joueur2);
scanf("%d", &depart_y);
printf("%s Entrez l'ordonnee de depart du pion : ", joueur2);
scanf("%d", &depart_x);
printf("\n\n");
printf("%s Entrez labcisse d'arrivee du pion : ", joueur2);
scanf("%d", &arrivee_y);
printf("%s Entrez l'ordonnee d'arrivee du pion : ", joueur2);
scanf("%d", &arrivee_x);
printf("\n\n");
if (verification(depart_x, depart_y, arrivee_x, arrivee_y, pion, Damier2) == 1)
{
Damier2[arrivee_x][arrivee_y]=Damier2[depart_x][depart_y];
Damier2[depart_x][depart_y] = 0;
}
//system("cls");
afficher_plateau(Damier2);
}


}


void alternance()
{
int i =0;
while(fin_de_partie !=1) // tant que aucun joueur a remporter la partie
{
if(i%2 == 0) // systeme pour faire 1 fois sur 2
{
jeu(1);
i++;
}
else
{
jeu(2);
i++;
}
}

}

void partie_multi(){
initialisation_plateau(Damier2);
configuration_piont(Damier2);
menu();
afficher_plateau(Damier2);
//color(15,0);
alternance();
}
